// Nada: mostrará todo el contenido del directorio actual
// Fichero: mostrará el fichero
// Directorio: mostrará el contenido del directorio (a menos que use -d)
// Ficheros: archivo (texto, script, código fuente...), directorio (carpetas)

import gnu.getopt.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.UserPrincipal;
import java.text.SimpleDateFormat;
import java.util.*;

public abstract class jls implements Path {

	private static final String RESET = "\u001B[0m";
	private static final String BLUE = "\u001B[34m";

	private static boolean a_opt = false; // Se mostrarán los ficheros ocultos
	private static boolean c_opt = false; // Los ficheros se mostrarán con colores
	private static boolean d_opt = false; // Se mostrará el nombre del directorio
	private static boolean l_opt = false; // Listado largo del fichero
	private static boolean r_opt = false; // Mostrar los ficheros en orden inverso
	//private static boolean R_opt = false; // Mostrar los ficheros recursivamente



	/** 
	 * Muestra por stdout el error producido y finaliza el programa
	 * @param err String con el mensaje de error
	 */
	public static void error (String err) {
		System.err.println (err);
		System.exit (0);
	}



	/** 
	 * DEBUG: muestra por stdout un array dado
	 * @param arrLis Array de ficheros
	 */
	public static void debugArray (ArrayList<File> arrLis) {

		for (File f: arrLis) {
			System.out.print ("\t"+ f.getName() +"\n");
		}
	}



	/** 
	 * DEBUG: muestra por stdout un string dado
	 * @param cadena String que contiene el nombre de la variable y su valor
	 */
	public static void debugPrint (String cadena) {
		System.out.println (cadena);
	}



	/** 
	 * Muestra la ayuda del programa
	 */
	public static void mostrarHelp() {
		System.out.println ("Uso: jls [acdhlr] [FICHERO]\n"
							+"Lista información sobre los FILEs (directorio actual por defecto).\n"
							+"Ordena los ficheros según el orden natural\n\n"
							+"  -a\tno ignora ficheros que comiencen por .\n"
							+"  -c\tcolorea el output\n"
							+"  -d\tmuestra el directorio en si mismo y no el contenido\n"
							+"  -h\tmuestra esta ayuda y finaliza la ejecución\n"
							+"  -l\tformato de listado largo\n"
							+"  -r\tinvierte el orden de mostrado\n"
							+"BNG joreutils");
		System.exit (0);
	}



	public static String getPermisos (File fichero) {
		char[] permisos = new char[3];

		permisos[0] = fichero.canRead() ? 'r' : '-';
		permisos[1] = fichero.canWrite() ? 'w' : '-';
		permisos[2] = fichero.canExecute() ? 'x' : '-';

		return new String (permisos);
	}



	/**
	 * Calcula el número de links o directorios de un fichero. Un directorio
	 * tendrá por defecto 2 por ". .."
	 * @param fichero Fichero (archivo o directorio)
	 * @return el número de links o directorios de un fichero
	 */
	public static int getLinksDirectorios (File fichero) {
		int num;
		File[] subFicheros = fichero.listFiles();
		
		if (fichero.isDirectory()) {
			num = 2;

			// Contador de subdirectorios
			for (File f: subFicheros) {
				if (f.isDirectory()) num++;
			}
		}
		else {
			num = 1;	// Es un fichero
		}

		return num;
	}



	public static String getPropietario (File fichero) {
		UserPrincipal usuario = null;

		try {
			Path fichPath = Paths.get(fichero.getName());	// Devuelve la ruta del fichero
			usuario = Files.getOwner (fichPath);
		}
		catch (IOException ioe) {
			System.err.println ("getPropietario(): "+ ioe.getMessage());
		}

		return usuario.getName();
	}



	public static String getUltimaModificacion (File fichero) {
		Date fecha = new Date (fichero.lastModified());
		SimpleDateFormat sdf = new SimpleDateFormat ("MMM d HH:m");

		return sdf.format (fecha);
	}



	/**
	 * Método que muestra las propiedades del fichero
	 * @param fichero Fichero (archivo o directorio)
	 * @return un String con todos los campos del listado largo
	 */
	public static String listadoLargo (File fichero) {
		char tipo; 			// Tipo de fichero
		String permisos; 	// Read, Write, eXecute
		int linksDirs; 		// Número de links o directorios
		String usuario; 	// Usuario propietario
		long tamanio; 		// Tamaño del fichero
		String ultimaMod;	// Última modificación

		tipo = fichero.isDirectory() ? 'd' : '-';	// Es un directorio
		permisos = getPermisos (fichero);				// Devuelve los permisos
		linksDirs = getLinksDirectorios (fichero);		// Devuelve el número de links o directorios
		usuario = getPropietario (fichero);				// Devuelve el usuario propietario
		tamanio = fichero.length();						// Devuelve el tamaño
		ultimaMod = getUltimaModificacion (fichero);	// Devuelve la última modificación

		//debugPrint (tipo +""+ permisos +" "+ linksDirs +" "+ usuario);
		return tipo +""+ permisos +" "+ linksDirs +" "+ usuario +" "+ tamanio +" "+ ultimaMod;
	}



	/** 
	 * Muestra por stdout los ficheros almacenados en "arrLis" en orden natural. 
	 * Al terminar, cierra la ejecución del programa
	 * @param arrLis Ficheros a ser mostrados
	 */
	public static void mostrarFicheros (ArrayList<File> arrLis) {

		arrLis.trimToSize();	// Ajusta el tamaño del ArrayList al contenido

		if (r_opt) { // Ordenar de forma inversa
			Collections.sort (arrLis, Collections.reverseOrder());
		}
		else {
			Collections.sort (arrLis);
		}

		for (File f: arrLis) {

			if (! f.exists()) { // Muestra el error si el fichero no existe
				System.err.println ("jls: cannot access '"+ f.getName() +"': No such file or directory");
				continue;
			}

			if (a_opt) { // Mostrar ficheros ocultos

				//if (c_opt && f.isDirectory()) System.out.print (BLUE); // Output azul

				if (l_opt) {
					System.out.print (listadoLargo (f) +" ");
					if (c_opt && f.isDirectory()) System.out.print (BLUE); // Output azul
					System.out.print (f.getName() +"\n");
				}
				else {
					if (c_opt && f.isDirectory()) System.out.print (BLUE); // Output azul
					System.out.print (f.getName() +"\t");
				}
			}
			else {
				if (! (f.getName().charAt (0) == '.')) {

					if (c_opt && f.isDirectory()) System.out.print (BLUE); // Output azul

					if (l_opt) {
						System.out.print (listadoLargo (f) +" ");
						if (c_opt && f.isDirectory()) System.out.print (BLUE); // Output azul
						System.out.print (f.getName() +"\n");
					}
					else {
						if (c_opt && f.isDirectory()) System.out.print (BLUE); // Output azul
						System.out.print (f.getName() +"\t");
					}
				}
			}
			System.out.print (RESET); // Resetear el color
		}
		System.out.println ("");
		System.exit (0);
	}



	/** 
	 * Formatear stdout para mostrar el contenido de los ficheros o el nombre 
	 * los directorios (d_opt = true)
	 * @param arrLis Objeto ArrayList uno o varios directorios 
	 * @param varios Indica si hay uno o varios directorios en "arrLis"
	 */
	public static void mostrarContenido (ArrayList<File> arrLis, boolean varios) {
		ArrayList<File> subFicheros = new ArrayList<File>();

		arrLis.trimToSize();	// Ajusta el tamaño del ArrayList al contenido
		//if (a_opt) System.out.print (".\t..\t");
		
		if (d_opt) mostrarFicheros (arrLis); // Mostrar el nombre de los directorios

		if (varios) {

			for (File f: arrLis) {

				if (! f.canRead()) {
					error ("jls: cannot open directory '"+ f.getName() +"': Permission denied");
				}

				System.out.println ("\n"+ f.getName() +":");
				subFicheros.addAll (Arrays.asList (f.listFiles()));	// Agrega los sub ficheros
				mostrarFicheros (subFicheros);						// Muestra sub ficheros
				subFicheros.clear(); 								// Limpiar los sub ficheros
			}
		}
		else {

			if (! arrLis.get (0).canRead()) {
				error ("jls: cannot open directory '"+ arrLis.get (0).getName() +"': Permission denied");
			}

			subFicheros.addAll (Arrays.asList (arrLis.get (0).listFiles()));	// Aprega los sub ficheros
			mostrarFicheros (subFicheros);										// Muestra los sub ficheros
		}
	}



	/** 
	 * Rellena "arrLis" con argumentos no-Getopt almacenados en "args". "inicioArgs" indica
	 * el comienzo de los ficheros y "opt" si hay alguna opción Getopt en el array
	 * @param arrLis Objeto ArrayList a rellenar de ficheros
	 * @param inicioArgs Posición del primer fichero en "args"
	 * @param args Array con todos los parámetros pasados a main
	 * @param opt true si hay opcines Getopt, false si no hay
	 */
	public static void arrayFicheros (ArrayList<File> arrLis, int inicioArgs, String[] args, boolean opt) {

		//debugPrint ("inicioArgs: "+ inicioArgs);
		//debugPrint ("args.length: "+ args.length);
		if (opt) {
			// Hay Getopt
			for (int cont = inicioArgs; cont <= (args.length - inicioArgs); cont++) {
				//debugPrint ("cont: "+ cont);
				arrLis.add (new File (args[cont]));
			}
		}
		else {
			// No hay Getopt
			for (int cont = inicioArgs; cont < (args.length - inicioArgs); cont++) {
				//debugPrint ("cont: "+ cont);
				arrLis.add (new File (args[cont]));
			}
		}
		//debugArray (arrLis);
	}



	/** 
	 * Devuelve los directorios almacenados en "arrLis"
	 * @param arrLis Objeto ArrayList con todos los ficheros almacenados
	 * @return objeto ArrayList relleno con directorios
	 */
	public static ArrayList<File> arrayDirectorios (ArrayList<File> arrLis) {
		ArrayList<File> directorios = new ArrayList<File>();

		for (File f: arrLis) {

			if (f.isDirectory()) {
				directorios.add (f);
			}
		}

		return directorios;
	}



	/** 
	 * Devuelve los archivos almacenados en "arrLis"
	 * @param arrLis Objeto ArrayLista con todos los ficheros almacenados
	 * @return objeto ArrayList relleno con archivos
	 */
	public static ArrayList<File> arrayArchivos (ArrayList<File> arrLis) {
		ArrayList<File> archivos = new ArrayList<File>();

		for (File f: arrLis) {

			if (! f.isDirectory()) {
				archivos.add (f);
			}
		}

		return archivos;
	}



	public static void main (String args[]) {

		int c;
		boolean opt; // T: hay getopt, F: no hay getopt
		String arg;
		Getopt option = new Getopt ("jls", args, "acdhlrR");
		while ((c = option.getopt()) != -1) {
			switch (c) {
				case 'a':
					a_opt = true;
					break;
				case 'c':
					c_opt = true;
					break;

				case 'd':
					d_opt = true;
					break;

				case 'h':
					mostrarHelp();
					break;

				case 'l':
					l_opt = true;
					break;

				case 'r':
					r_opt = true;
					break;
					
					/*
				case 'R':
					R_opt = true;
					break;
					*/
			}
		}

		ArrayList<File> ficheros = new ArrayList<File>();	// ArrayList de ficheros
		File path = null;									// Path a un fichero
		int inicioArgs = option.getOptind(); 				// Primer argumento no Getopt
		opt = inicioArgs > 0 ? true : false;				// Hay o no hay getopts?
		arrayFicheros (ficheros, inicioArgs, args, opt); 	// Rellena ficheros con ficheros


		// Sin parámetros: mostrar todos los ficheros del directorio actual
		if (ficheros.size() == 0) { 
			path = new File ("./");

			if (d_opt) { // Muestra el nombre del directorio actual
				if (c_opt && path.isDirectory()) System.out.print (BLUE); // Output azul
				System.out.println (path.getName() + RESET);
				System.exit (0);
			}

			// Comprobar si se puede leer
			if (path.canRead()) { 
				ficheros.addAll (Arrays.asList (path.listFiles()));
				mostrarFicheros (ficheros);
			}
			else {
				error ("jls: cannot open directory '.': Permission denied");
			}
		}
		// Con parámetros 1,1
		else if (ficheros.size() == 1) {

			if (ficheros.get (0).isDirectory()) {
				mostrarContenido (ficheros, false); // Mostrar el contenido de un directorio
			}
			else {
				mostrarFicheros (ficheros); // Mostrar el nombre de un archivo
			}
		}

		// 2,n ficheros
		else { 

			ficheros.trimToSize();	// Ajusta el tamaño al contenido
			ArrayList<File> directorios = arrayDirectorios (ficheros);
			ArrayList<File> archivos = arrayArchivos (ficheros);

			if (! (archivos.size() == 0)) {
				mostrarFicheros (archivos);
			}

			if (! (directorios.size() == 0)) {
				mostrarContenido (directorios, true); // Mostrar varios directorios
			}
		}
	}
}
